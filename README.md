<h1 align="center">Hello 👋</h1>
<img width=100% src="https://github-readme-stats.vercel.app/api?username=abhj&show_icons=true" />
<img width=100% src="https://github-readme-stats.vercel.app/api/top-langs/?username=abhj&hide=html,css&layout=compact&langs_count=4" />
<img width=100% src="https://github-readme-stats.vercel.app/api/wakatime?username=abhj&custom_title=Today's Coding Stats" />
<img width=100% src="https://wakatime.com/share/@abhj/9ebae3c8-e8a4-4b7d-8ed1-0376174b903e.svg" /> 
<img width=100% src="https://wakatime.com/share/@abhj/49a32179-f0e2-4c91-8f03-b5aa54518929.svg" />  
I am a Final Year Undergraduate of Electrical Engineering @ IIT Kharagpur and aspiring Software Engineer.  
  
I am quite passionate about Competitive Programming.  I am a quick learner, a chess lover, and an avid reader.  
  
I am probably an ISTJ (https://www.16personalities.com/profiles/cd5350afa084b) and certainly a minimalist and perfectionist.  
  
<!-- <img width=100% src="https://github.com/AbhJ/abhj/raw/master/image/pers.png"></img>  -->
My public portfolio page is https://abhj.github.io/public-portfolio and my blog page is https://abhj.github.io/abj-blogs where you may know more about me.  
  
<!-- My wakatime profile is https://wakatime.com/@abhj.   -->
Apart from software and math, stuff I enjoy are reading lots of books (https://www.goodreads.com/abhj), watching loads of anime (https://myanimelist.net/animelist/abhj), reading manga (https://myanimelist.net/mangalist/abhj), listening to David Guetta (and Ricky Martin) and watching true crime investigation documentaries for hours.

<hr>  
<span align="center">

[<img src="https://toppng.com/uploads/preview/letter-a-png-11552941435fskk9pyn7o.png" width="3.5%"/>](https://abhj.github.io)  &nbsp; 
[<img src="https://img.icons8.com/color/48/000000/twitter.png" width="3.5%"/>](http://www.twitter.com/abjkgp)  &nbsp; 
[<img src="https://img.icons8.com/color/48/000000/linkedin.png" width="3.5%"/>](https://www.linkedin.com/in/abhj)  &nbsp; 
[<img src="https://img.icons8.com/fluent/48/000000/facebook-new.png" width="3.5%"/>](http://www.facebook.com/abhijaymitra)  &nbsp; 
[<img src="https://img.icons8.com/fluent/48/000000/instagram-new.png" width="3.5%"/>](https://www.instagram.com/abjkgp)  &nbsp; 
</span>
